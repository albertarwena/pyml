"""
Quiz3
"""

# q1
acc1 = 1 - 1/100

# confusion matrix
tp = 96
fp = 8
tn = 19
fn = 4

# q2 accuracy
acc2 = (tn+tp)/float(tn+tp+fn+fp)
print('acc: {}'.format(round(acc2, 3)))

# q3 precision
prec = tp/float(fp+tp)
print('precision: {}'.format(round(prec, 3)))

# q4 recall
rec = tp/float(tp+fn)
print('recall: {}'.format(round(rec, 3)))

# q5 precisson-recall curve for model m, given X_test and y_test
from sklearn.metrics import precision_recall_curve
from pandas import DataFrame

y_score = m.decision_function(X_test)
precision, recall, _ = precision_recall_curve(y_test, y_score)

df = DataFrame({'precision' : precision, 'recall' : recall})
print(df)

# q8 calculate macro precission score for model m
from sklearn.metrics import precision_score
y_pred = m.predict(X_test)
macro_precision = precision_score(y_test, y_pred, average='macro')
print('macro precision {}'.format(macro_precision))

# q 13 compute recall - precision
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import precision_score, recall_score
param_grid = {'C':  [0.01, 0.1, 1, 10], 'gamma' :  [0.01, 0.1, 1, 10]}
gs = GridSearchCV(m, param_grid, scoring='recall')

gs.fit(X_test, y_test)

y_pred = gs.predict(X_test)

prec = precision_score(y_test, y_pred)
rec = recall_score(y_test, y_pred)
dif = prec - rec

print('precission - recall = {}'.format(dif))

# q14 precision - recall
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import precision_score, recall_score
param_grid = {'C':  [0.01, 0.1, 1, 10], 'gamma' :  [0.01, 0.1, 1, 10]}
gs = GridSearchCV(m, param_grid, scoring='precision')

gs.fit(X_test, y_test)

y_pred = gs.predict(X_test)

prec = precision_score(y_test, y_pred)
rec = recall_score(y_test, y_pred)
dif = prec - rec

print('precission - recall = {}'.format(dif))